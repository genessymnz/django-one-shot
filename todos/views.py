from django.shortcuts import get_object_or_404, redirect, render
from todos.forms import TodoForm
from todos.models import TodoList

# Create your views here.
def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list": list,
    }
    return render(request, "todos/todolist.html", context)

def todo_list_detail(request, id):
    todo_detail = TodoList.objects.get(id=id)
    context = {
        "todo_detail" : todo_detail,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
      # To redirect to the detail view of the model, use this:
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_list_edit(request, id):
    todolist = get_object_or_404(TodoForm, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail",id=id)

    else:
        form = TodoForm(instance=todolist)

    context = {
        "todo_object": todolist,
        "form": form,
    }
    return render(request, "todos/edit.html", context)
